#!/usr/bin/env python3
import setuptools

setuptools.setup(
    name='nextcloud_update',
    version='1.0',
    scripts=['nextcloud_update'],
    description='NextCloud update script for syncing files '
                'from cloud server to interactive',
    url='https://gitlab.com/naturalis/mii/nextcloud-update',
    author='Joep Vermaat',
    author_email='joep.vermaat@naturalis.nl',
    zip_safe=False,
    install_requires=[
        "pyocclient >= 0.4"
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: LGPL License",
        "Operating System :: OS Independent",
    ],
)
