# Nextcloud Update

This basic script syncs files from specified paths on 
a nextcloud or owncloud fileserver. It can be used for 
publishing files on remote servers whenever a file or 
directory changes the files are downloaded and overwritten.

## Usage 

    nextcloud_update nexcloud/path/to/file [./destpath/[file]]

or

    nextcloud_update nexcloud/path/to/path/ [./destpath/]

Downloads a nextcloud file

### positional arguments

    paths                Remote path on the nextcloud instance

    optional arguments:
        -h, --help           show this help message and exit
        --server SERVER      Cloud server url
        --user USER          Cloud server user
        --password PASSWORD  Cloud server password
        --config CONFIG      Cloud server config (json) file

## Install

```
pip3 install -e git+https://gitlab.com/Naturalis/mii/nextcloud-update#egg=nextcloud-update
```

## Functions

### check_tag(key)

Check the path of a file to the etag hash table.
        
    :param key:
    :return etag:
    
### connect_to_cloud(url, user, password)

Connect to the cloud server
        
    :param url:
    :param user:
    :param password:
    :return
    
### get_file(cloudPath='', destPath='./')

Downloads the file from the remote server
        
    
### get_folder(cloudPath='', destPath='./')

Get all the files of complete folder on the remote server
        
    
### get_tags()

Loads the etag json hash file containing the etags of files on the remote server.
        
    :return tags:
    
### main()

The main function reading commandline parameters. Getting
login credentials. Logging in. Getting the file or remote folder.
    
### read_config(configFile=None)

Reads the cloud server configuration
        
    :return configuration:
    
### store_tag(key, value)

Stores the path of a file and it's etag to the etag hash table.
        
    :param key:
    :param value:
    :return tags:
    
### store_tags(tags=None)

Stores the etag hash table into a json file containing the etags of files on the remote server.
        
    :param tags:
